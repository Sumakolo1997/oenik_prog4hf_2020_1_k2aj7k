﻿namespace CloneArmy.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO.Compression;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AutoMapper;
    using Clone_Army.Data;
    using Clone_Army.Logic;
    using Clone_Army.Repository;
    using CloneArmy.Web.Models;

    public class CloneApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;

        public CloneApiController()
        {
            CloneArmyDataEntities de = new CloneArmyDataEntities();
            Clone_Repository clone = new Clone_Repository();
            Army_Repository army = new Army_Repository();
            Class_Repository classes = new Class_Repository();
            this.logic = new Logic();
            this.mapper = MapperFactory.CreateMapper();
        }

        //Get api/CloneApi
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Clone> GetAll()
        {
            var clones = this.logic.GetAllCloneData();
            return this.mapper.Map<IList<Clone_Army.Data.Clone>, List<Models.Clone>>(clones);
        }

        //GET api/CloneApi/del/valami
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneClone(int id)
        {
            bool success = logic.DeleteCloneData(id);
            return new ApiResult()
            {
                OperationResult = success
            };
        }

        //POST api/CloneApi/add + clone
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneClone(CloneArmy.Web.Models.Clone clone)
        {
            var clones = this.logic.GetAllCloneData();
            int id = 1;
            foreach (var item in clones)
            {
                if (item.Clone_ID == id)
                {
                    id++;
                }
            }

            logic.InsertCloneData(id, clone.Class_ID, clone.Army_ID, clone.NickName, clone.Name_, clone.Speciality, clone.Birth_Date, clone.Ranks);           

            return new ApiResult()
            {
                OperationResult = true
            };
        }

        //POST api/CloneApi/mod + clone
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneClone(CloneArmy.Web.Models.Clone clone)
        {
            bool success = logic.UpdateCloneData(clone.Clone_ID, clone.Clone_ID, clone.Class_ID, clone.Army_ID, clone.NickName, clone.Name_, clone.Speciality, clone.Birth_Date, clone.Ranks);
            return new ApiResult()
            {
                OperationResult = success
            };
        }
    }
}
