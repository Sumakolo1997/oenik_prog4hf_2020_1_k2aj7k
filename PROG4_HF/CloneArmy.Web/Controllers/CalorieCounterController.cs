﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kaloriaszamlalo.Models;

namespace Kaloriaszamlalo.Controllers
{
    public class CalorieCounterController : Controller
    {
        public class Exercise
        {
            public string Name { get; set; }
            public double Burn { get; set; }
            public Exercise(string name, double burn)
            {
                Name = name;
                Burn = burn;
            }
        }
        // GET: CalorieCounter
        // GET: /CalorieCounter/CalorieCount
        public ActionResult CalorieCount()
        {
            return View("Input");
        }
        [HttpPost]
        public ActionResult CalorieCount(Input input)
        {
            List<Exercise> exercises = new List<Exercise>();
            exercises.Add(new Exercise("Running", 1000));
            exercises.Add(new Exercise("Yoga", 400));
            exercises.Add(new Exercise("Pilates", 472));
            exercises.Add(new Exercise("Hiking", 700));
            exercises.Add(new Exercise("Swimming", 1000));
            exercises.Add(new Exercise("Bicycle", 600));

            Exercise f = null;
            foreach (Exercise exercise in exercises)
            {
                if (exercise.Name == input.exercise)
                {
                    f = new Exercise(exercise.Name, exercise.Burn);
                }
            }
            
            
            Output Op = new Output()
            {
                Name = input.Name,
                Weight = input.Weight,
                Length = input.Length,
                BurntCalorie = input.Weight * (input.Length/60) * (f.Burn/100)
            };
            return View("Output", Op);
            
        }
    }
}