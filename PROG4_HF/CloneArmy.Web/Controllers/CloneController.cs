﻿namespace CloneArmy.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using Clone_Army.Data;
    using Clone_Army.Logic;
    using Clone_Army.Repository;
    using CloneArmy.Web.Models;

    /// <summary>
    /// The main controller of the api.
    /// </summary>
    public class CloneController : Controller
    {
        private ILogic logic;
        private IMapper mapper;
        private CloneViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloneController"/> class.
        /// </summary>
        public CloneController()
        {
            CloneArmyDataEntities de = new CloneArmyDataEntities();
            Clone_Repository clone = new Clone_Repository();
            Army_Repository army = new Army_Repository();
            Class_Repository classes = new Class_Repository();
            this.logic = new Logic();
            this.mapper = MapperFactory.CreateMapper();
            this.vm = new CloneViewModel();
            this.vm.EditedClone = new CloneArmy.Web.Models.Clone();
            var clones = logic.GetAllCloneData();
            this.vm.CloneList = this.mapper.Map<IList<Clone_Army.Data.Clone>, List<Models.Clone>>(clones);
        }

        private CloneArmy.Web.Models.Clone GetCloneModel(int id)
        {
            Clone_Army.Data.Clone clone = this.logic.GetClone(id);
            return this.mapper.Map<Clone_Army.Data.Clone, Models.Clone>(clone);
        }

        /// <summary>
        /// Returns the Index page.
        /// </summary>
        /// <returns>ActionResult.</returns>
        // GET: Clone
        public ActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("CloneIndex", this.vm);
        }

        /// <summary>
        /// Returns the Details page.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>ActionResult.</returns>
        // GET: Clone/Details/5
        public ActionResult Details(int id)
        {
            return this.View("CloneDetail", this.GetCloneModel(id));
        }

        /// <summary>
        /// Returns the Remove page.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>ActionResult.</returns>
        // GET: /Clone/Remove/5
        public ActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL";
            if (this.logic.DeleteCloneData(id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction("Index");
        }

        // GET: /Clone/Edit/5
        public ActionResult Edit (int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedClone = this.GetCloneModel(id);
            return this.View("CloneIndex", this.vm);
        }

        /// <summary>
        /// Returns the Edit page.
        /// </summary>
        /// <param name="clone">clone.</param>
        /// <param name="editAction">editaction.</param>
        /// <returns>ActionResult.</returns>
        // POST /Clone/Edit + Clone + editAction
        [HttpPost]
        public ActionResult Edit(CloneArmy.Web.Models.Clone clone, string editAction)
        {
            if (this.ModelState.IsValid && clone != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    this.logic.InsertCloneData(clone.Clone_ID, clone.Class_ID, clone.Army_ID, clone.NickName, clone.Name_, clone.Speciality, clone.Birth_Date, clone.Ranks);
                }
                else
                {
                    bool success = this.logic.UpdateCloneData(clone.Clone_ID, clone.Clone_ID, clone.Class_ID, clone.Army_ID, clone.NickName, clone.Name_, clone.Speciality, clone.Birth_Date, clone.Ranks);
                    if (!success)
                    {
                        this.TempData["editResult"] = "Edit FAIL";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedClone = clone;
                return this.View("CloneIndex", this.vm);
            }
        }
    }
}
