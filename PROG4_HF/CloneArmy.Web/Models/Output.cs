﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kaloriaszamlalo.Models
{
    public class Output
    {
        public string Name { get; set; }

        public double Weight { get; set; }

        public double Length { get; set; }

        public double BurntCalorie { get; set; }
    }
}