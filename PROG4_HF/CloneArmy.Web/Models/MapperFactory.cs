﻿namespace CloneArmy.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using AutoMapper;

    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Clone_Army.Data.Clone,
                    CloneArmy.Web.Models.Clone>().
                    ForMember(dest => dest.Clone_ID, map => map.MapFrom(src => src.Clone_ID)).
                    ForMember(dest => dest.Class_ID, map => map.MapFrom(src => src.Class_ID)).
                    ForMember(dest => dest.Army_ID, map => map.MapFrom(src => src.Army_ID)).
                    ForMember(dest => dest.NickName, map => map.MapFrom(src => src.NickName)).
                    ForMember(dest => dest.Name_, map => map.MapFrom(src => src.Name_)).
                    ForMember(dest => dest.Speciality, map => map.MapFrom(src => src.Speciality)).
                    ForMember(dest => dest.Birth_Date, map => map.MapFrom(src => src.Birth_Date)).
                    ForMember(dest => dest.Ranks, map => map.MapFrom(src => src.Ranks)).
                    ForMember(dest => dest.ArmyName, map => map.MapFrom(src => src.Army == null ? string.Empty : src.Army.Name_)).
                    ForMember(dest => dest.ClassName, map => map.MapFrom(src => src.Class == null ? string.Empty : src.Class.Name_));
            });

            return config.CreateMapper();
        }
    }
}