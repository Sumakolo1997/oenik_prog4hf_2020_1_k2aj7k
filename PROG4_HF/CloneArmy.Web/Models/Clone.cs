﻿namespace CloneArmy.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;
    using Clone_Army.Data;

    /// <summary>
    /// Form model.
    /// </summary>
    public class Clone
    {
        /// <summary>
        /// Gets or sets the clone id.
        /// </summary>
        [Display(Name ="Clone Id")]
        [Required]
        public int Clone_ID { get; set; }

        /// <summary>
        /// Gets or sets the class id.
        /// </summary>
        [Display(Name = "Class Id")]
        [Required]
        public int Class_ID { get; set; }

        /// <summary>
        /// Gets or sets the armmy id.
        /// </summary>
        [Display(Name = "Army Id")]
        [Required]
        public int Army_ID { get; set; }

        /// <summary>
        /// Gets or sets the nickname of the clone.
        /// </summary>
        [Display(Name = "NickName")]
        [Required]
        public string NickName { get; set; }

        /// <summary>
        /// Gets or sets the name of the clone.
        /// </summary>
        [Display(Name = "Clone Serial Number")]
        [Required]
        [StringLength(10, MinimumLength = 7)]
        public string Name_ { get; set; }

        /// <summary>
        /// Gets or sets the birth date of the clone.
        /// </summary>
        [Display(Name = "Clone Birth date")]
        [Required]
        public string Birth_Date { get; set; }

        /// <summary>
        /// Gets or sets the speciality of the clone.
        /// </summary>
        [Display(Name = "Speciality")]
        [Required]
        public string Speciality { get; set; }

        /// <summary>
        /// Gets or sets the rank of the clone.
        /// </summary>
        [Display(Name = "The rank of the clone")]
        [Required]
        public string Ranks { get; set; }

        /// <summary>
        /// Gets or sets the army of the clone.
        /// </summary>
        [Display(Name = "The clone's army")]
        public virtual string ArmyName { get; set; }

        /// <summary>
        /// Gets or sets the class of the clone.
        /// </summary>
        [Display(Name = "The clone's class")]
        public virtual string ClassName { get; set; }
    }
}