﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Kaloriaszamlalo.Controllers.CalorieCounterController;

namespace Kaloriaszamlalo.Models
{
    public class Input
    {
        public string Name { get; set; }

        public double Weight { get; set; }

        public string exercise { get; set; }

        public double Length { get; set; }  
    }
}