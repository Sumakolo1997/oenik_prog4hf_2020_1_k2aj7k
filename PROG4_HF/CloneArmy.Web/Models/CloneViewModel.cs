﻿namespace CloneArmy.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Get the CloneViewModel.
    /// </summary>
    public class CloneViewModel
    {
        /// <summary>
        /// Gets or sets a clone that is edited.
        /// </summary>
        public Clone EditedClone { get; set; }

        /// <summary>
        /// Gets or sets a list of the clones in the database.
        /// </summary>
        public List<Clone> CloneList { get; set; }
    }
}