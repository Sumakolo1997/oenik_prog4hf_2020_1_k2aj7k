﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Clone_Army.Data;
    using Clone_Army.Logic;
    using Clone_Army.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// LogicTest is the main class which handles Tests for the Logic class.
    /// It uses MOQ, and NUNIT3 Adapters to test method calls with injection, and such.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<Logic> m2;
        private Mock<IRepository<Army>> mockedArmyRepo;
        private Mock<IRepository<Clone>> mockedCloneRepo;
        private Mock<IRepository<Class>> mockedClassRepo;

        /// <summary>
        /// Sets up the mocked repositories before the tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Logic m = Mock.Of<Logic>();
            this.m2 = Mock.Get(m);
            this.mockedArmyRepo = new Mock<IRepository<Army>>();
            this.mockedClassRepo = new Mock<IRepository<Class>>();
            this.mockedCloneRepo = new Mock<IRepository<Clone>>();
            List<Army> armies = new List<Army>()
            {
                new Army() { Army_ID = 111, Name_ = "ArmyOfYolo", Color = "blue", Date_Of_Founding = "nooneknows", HeadCount = 600000, Specialization = "eating", Name_Of_General = "Torma László" },
                new Army() { Army_ID = 112, Name_ = "XForce", Color = "red", Date_Of_Founding = "a long time ago", HeadCount = 123456, Specialization = "drinking", Name_Of_General = "Someone" },
                new Army() { Army_ID = 113, Name_ = "XForce", Color = "red", Date_Of_Founding = "a long time ago", HeadCount = 123456, Specialization = "drinking", Name_Of_General = "Someone" },
                new Army() { Army_ID = 114, Name_ = "XForce", Color = "red", Date_Of_Founding = "a long time ago", HeadCount = 123456, Specialization = "drinking", Name_Of_General = "Someone" },
                new Army() { Army_ID = 115, Name_ = "XForce", Color = "red", Date_Of_Founding = "a long time ago", HeadCount = 123456, Specialization = "drinking", Name_Of_General = "Someone" }
            };

            List<Clone> clones = new List<Clone>()
            {
                new Clone() { Army_ID = 111, Clone_ID = 121, Class_ID = 132, Birth_Date = "not_today", Name_ = "Sandor", NickName = "Béla", Speciality = "eating", Ranks = "Marvel" },
                new Clone() { Army_ID = 112, Clone_ID = 125, Class_ID = 133, Birth_Date = "sometimes", Name_ = "Noone", NickName = "Arya", Speciality = "plotArmor", Ranks = "everything" },
                new Clone() { Army_ID = 113, Clone_ID = 124, Class_ID = 133, Birth_Date = "sometimes", Name_ = "Noone", NickName = "Arya", Speciality = "plotArmor", Ranks = "everything" },
                new Clone() { Army_ID = 114, Clone_ID = 123, Class_ID = 133, Birth_Date = "sometimes", Name_ = "Noone", NickName = "Arya", Speciality = "plotArmor", Ranks = "Commander" },
                new Clone() { Army_ID = 115, Clone_ID = 122, Class_ID = 132, Birth_Date = "tomorrow", Name_ = "Peter", NickName = "Parker", Speciality = "swinging", Ranks = "The Best" },
            };

            List<Class> classes = new List<Class>()
            {
                new Class() { Class_ID = 132, Name_ = "Dayum", Main_Weapon = "steel balls", Special_Training = "yes", Utility = "cloak", Rarity = "dunno lol" },
                new Class() { Class_ID = 133, Name_ = "Chad", Main_Weapon = "muscles", Special_Training = "no", Utility = "fine as heck", Rarity = "everywhere" },
                new Class() { Class_ID = 134, Name_ = "Chad", Main_Weapon = "muscles", Special_Training = "no", Utility = "fine as heck", Rarity = "everywhere" },
                new Class() { Class_ID = 135, Name_ = "Chad", Main_Weapon = "muscles", Special_Training = "no", Utility = "fine as heck", Rarity = "everywhere" },
                new Class() { Class_ID = 136, Name_ = "Chad", Main_Weapon = "muscles", Special_Training = "no", Utility = "fine as heck", Rarity = "everywhere" }
            };

            this.m2.Setup(x => x.Helper.Army_Repository.List_Table()).Returns(armies.AsQueryable());
            this.m2.Setup(x => x.Helper.Clone_Repository.List_Table()).Returns(clones.AsQueryable());
            this.m2.Setup(x => x.Helper.Class_Repository.List_Table()).Returns(classes.AsQueryable());
            this.mockedArmyRepo.Setup(x => x.List_Table()).Returns(armies.AsQueryable());
            this.mockedCloneRepo.Setup(x => x.List_Table()).Returns(clones.AsQueryable());
            this.mockedClassRepo.Setup(x => x.List_Table()).Returns(classes.AsQueryable());
        }

        /// <summary>
        /// Checks if the clone insertion from logic is true.
        /// </summary>
        [Test]
        public void CloneInsertCheck()
        {
            Assert.That(this.m2.Object.InsertCloneData(126, 003, 005, "Bélus", "Péter", "drinking", "yesterday", "Marvel"), Is.True);
        }

        /// <summary>
        /// Cheks if the army insertion from logic is true.
        /// </summary>
        [Test]
        public void ArmyInsertCheck()
        {
            Assert.That(this.m2.Object.InsertArmyData(116, "ArmyOfDoom", 6000, "purple", "nooneknows", "bunnyhopping", "Torma László"), Is.True);
        }

        /// <summary>
        /// Ckecks if the class insertion from logic is true.
        /// </summary>
        [Test]
        public void ClassInsertCheck()
        {
            Assert.That(this.m2.Object.InsertClassData(134, "Dirr", "steel balls", "cd craks", "yes", "dunno lol"), Is.True);
        }

        /// <summary>
        /// Checks if the clone update from logic is true.
        /// </summary>
        [Test]
        public void CloneUpdateCheck()
        {
            Assert.That(this.m2.Object.UpdateCloneData(121, 121, 136, 111, "Béla", "László", "eating", "not_today", "Marvel"), Is.True);
        }

        /// <summary>
        /// Checks if the army update from logic is true.
        /// </summary>
        [Test]
        public void ArmyUpdateCheck()
        {
            Assert.That(this.m2.Object.UpdateArmyData(111, 111, "ArmyOfYolo", 500000, "blue", "nooneknows", "eating", "Torma László"), Is.True);
        }

        /// <summary>
        /// Checks if the class update from logic is true.
        /// </summary>
        [Test]
        public void ClassUpdateCheck()
        {
            Assert.That(this.m2.Object.UpdateClassData(132, 132, "Dayum", "steel balls", "cloak", "no", "dunno lol"), Is.True);
        }

        /// <summary>
        /// Checks if the specific clone exists.
        /// </summary>
        [Test]
        public void CheckIfCloneExistIsWorkingAsIntended()
        {
            Assert.That(this.m2.Object.CheckIfCloneExists(121), Is.True);
        }

        /// <summary>
        /// Checks if the specific army exists.
        /// </summary>
        [Test]
        public void CheckIfArmyExistIsWorkingAsIntended()
        {
            Assert.That(this.m2.Object.CheckIfArmyExists(112), Is.True);
        }

        /// <summary>
        /// Checks if the specific class exists.
        /// </summary>
        [Test]
        public void CheckIfClassExistIsWorkingAsIntended()
        {
            Assert.That(this.m2.Object.CheckIfClassExists(132), Is.True);
        }

        /// <summary>
        /// Checks if the specific army is deleted.
        /// </summary>
        [Test]
        public void CheckIfArmyDeleteHappened()
        {
            Assert.That(this.m2.Object.DeleteArmyData(112), Is.True);
        }

        /// <summary>
        /// Checks if the specific clone is deleted.
        /// </summary>
        [Test]
        public void CheckIfCloneDeleteHappened()
        {
            Assert.That(this.m2.Object.DeleteCloneData(121), Is.True);
        }

        /// <summary>
        /// Checks if the specific class is deleted.
        /// </summary>
        [Test]
        public void CheckIfClassDeleteHappened()
        {
            Assert.That(this.m2.Object.DeleteClassData(132), Is.True);
        }

        /// <summary>
        /// Checks if the list is not empty.
        /// </summary>
        [Test]
        public void CheckGettingTableNamesIsWorkingAsIntended()
        {
            Assert.That(this.m2.Object.GetAllTableNames(), Is.Not.Null);
        }

        /// <summary>
        /// Checks if we get the army table's contents.
        /// </summary>
        [Test]
        public void CheckGettingArmyDataAsStringIsWorkingAsIntended()
        {
            Assert.That(this.m2.Object.GetArmyData(), Is.Not.Null);
        }

        /// <summary>
        /// Checks if we get the class table's contents.
        /// </summary>
        [Test]
        public void CheckGettingClassDataAsStringIsWorkingAsIntended()
        {
            Assert.That(this.m2.Object.GetClassData(), Is.Not.Null);
        }

        /// <summary>
        /// Checks if we get the clone table's contents.
        /// </summary>
        [Test]
        public void CheckGettingCloneDataAsStringIsWorkingAsIntended()
        {
            Assert.That(this.m2.Object.GetCloneData(), Is.Not.Null);
        }

        /// <summary>
        /// Checks if there is any commanders among the clones.
        /// </summary>
        [Test]
        public void CheckIfThereIsACommanderOrNot()
        {
            Assert.That(this.m2.Object.CommandersName(this.mockedCloneRepo.Object), Is.Not.Null);
        }

        /// <summary>
        /// Checks if there is any class that gets special training.
        /// </summary>
        [Test]
        public void CheckIfClassesHadSpecialTraining()
        {
            Assert.That(this.m2.Object.HowManySpecialTraining(this.mockedClassRepo.Object), Is.AtLeast(0));
        }

        /// <summary>
        /// Checks that, the method gets a value.
        /// </summary>
        [Test]
        public void CheckThatAverageHeadCountGetsAValue()
        {
            Assert.That(this.m2.Object.GetAverageForHeadCountOfTheArmy(this.mockedArmyRepo.Object), Is.Not.Null);
        }
    }
}
