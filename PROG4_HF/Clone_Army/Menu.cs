﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Clone_Army.Repository;

    /// <summary>
    /// This is the class which handles the display, and base functions of the
    /// menu.
    /// </summary>
    public class Menu
    {
        private const string V = @"http://localhost:8080/OENIK_PROG3_2019_1_K2AJ7K/";

        private bool run = true;

        /// <summary>
        /// This method keeps the main thread alive, and displays the menu.
        /// </summary>
        internal void Watcher()
        {
            this.Help();
            while (this.run)
            {
                string s = Console.ReadLine();
                int i;
                bool b = int.TryParse(s, out i);
                while (!b)
                {
                    Console.WriteLine("Hibás Opció! Listázom a lehetőségeket.");
                    this.Help();
                    s = Console.ReadLine();
                    b = int.TryParse(s, out i);
                }

                Logic.Logic logic = new Logic.Logic();
                try
                {
                    switch (i)
                    {
                        case 1:
                            {
                                Console.WriteLine("Lisztázom az összes elérhető neveket. ");
                                List<string> list = logic.GetAllTableNames();
                                Console.WriteLine(string.Join(", ", list));
                                break;
                            }

                        case 2:
                            {
                                List<string> list = logic.GetAllTableNames();
                                Console.WriteLine("Írd be a tábla nevét amiből listázni szeretnél!");
                                string dbname = Console.ReadLine();
                                while (!list.Contains(dbname))
                                {
                                    Console.WriteLine("Írd be a tábla nevét amiből listázni szeretnél!");
                                    dbname = Console.ReadLine();
                                }

                                Console.WriteLine(dbname + " LISTÁZÁS.");
                                Console.WriteLine();
                                if (dbname == "Army")
                                {
                                    Console.WriteLine(logic.GetArmyData());
                                }
                                else if (dbname == "Clones")
                                {
                                    Console.WriteLine(logic.GetCloneData());
                                }
                                else if (dbname == "Classes")
                                {
                                    Console.WriteLine(logic.GetClassData());
                                }

                                break;
                            }

                        case 3:
                            {
                                List<string> list = logic.GetAllTableNames();
                                Console.WriteLine("Írd be a tábla nevét ahova hozzá kívánsz adni értéket!");
                                string dbname = Console.ReadLine();
                                while (!list.Contains(dbname))
                                {
                                    Console.WriteLine("Írd be a tábla nevét ahova hozzá kívánsz adni értéket!");
                                    dbname = Console.ReadLine();
                                }

                                if (dbname == "Army")
                                {
                                    Console.WriteLine("Adatbázisban lévő elemek: ");
                                    Console.WriteLine(logic.GetArmyData() + "\n");

                                    Console.WriteLine("Írja be a hadsereg ID-ját! (XYZ)");

                                    string army_ID = Console.ReadLine();
                                    int army_ID2;
                                    bool b1 = int.TryParse(army_ID, out army_ID2);
                                    while (!b1)
                                    {
                                        Console.WriteLine("Írj be egy ID-t! (XYZ)");
                                        army_ID = Console.ReadLine();
                                        b1 = int.TryParse(army_ID, out army_ID2);
                                    }

                                    Console.WriteLine("Írja be a hadsereg nevét!");
                                    string name_ = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg létszámát!");

                                    string headcount = Console.ReadLine();
                                    int headcount2;
                                    bool b2 = int.TryParse(headcount, out headcount2);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Írj be egy létszámot.");
                                        headcount = Console.ReadLine();
                                        b2 = int.TryParse(headcount, out headcount2);
                                    }

                                    Console.WriteLine("Írja be a hadsereg színét!");
                                    string color = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg alapításának évét! (ajánlott megadási mód: xy BBY)");
                                    string date_of_founding = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg specializációját!");
                                    string specialization = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg generáisának a nevét!");
                                    string name_of_general = Console.ReadLine();

                                    bool success = logic.InsertArmyData(army_ID2, name_, headcount2, color, date_of_founding, specialization, name_of_general);
                                    string text = success ? "Sikeres bevitel." : "Sikertelen bevitel";
                                    Console.WriteLine(text);
                                }
                                else if (dbname == "Clones")
                                {
                                    Console.WriteLine("Adatbázisban lévő klónok:\n");

                                    Console.WriteLine(logic.GetCloneData() + "\n");

                                    Console.WriteLine("Írd be a klón ID-ját! (xyz)");
                                    string clone_id = Console.ReadLine();
                                    int clone_id2;
                                    bool b1 = int.TryParse(clone_id, out clone_id2);
                                    while (!b1)
                                    {
                                        Console.WriteLine("Írd be a klassz ID-ját! (xyz)");
                                        clone_id = Console.ReadLine();
                                        b1 = int.TryParse(clone_id, out clone_id2);
                                    }

                                    Console.WriteLine("Adatbázisban lévő klasszok:\n");

                                    Console.WriteLine(logic.GetClassData() + "\n");

                                    ClassDoesntExist:
                                    Console.WriteLine("Írd be a klón klassz ID-ját! (xyz)");
                                    string class_id = Console.ReadLine();
                                    int class_id2;
                                    bool b2 = int.TryParse(class_id, out class_id2);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Írd be a klón klassz ID-ját! (xyz)");
                                        class_id = Console.ReadLine();
                                        b2 = int.TryParse(class_id, out class_id2);
                                    }

                                    if (!logic.CheckIfClassExists(class_id2))
                                    {
                                        Console.WriteLine("Nem létezik a klassz id!");
                                        goto ClassDoesntExist;
                                    }

                                    Console.WriteLine("Adatbázisban lévő hadseregek:\n");

                                    Console.WriteLine(logic.GetArmyData() + "\n");

                                    ArmyDoesntExist:
                                    Console.WriteLine("Írd be a hadsereg ID-jét! (xyz)");
                                    string army_id = Console.ReadLine();
                                    int army_id2;
                                    bool b3 = int.TryParse(army_id, out army_id2);
                                    while (!b3)
                                    {
                                        Console.WriteLine("Írd be a klón hadsereg ID-ját! (xyz)");
                                        army_id = Console.ReadLine();
                                        b2 = int.TryParse(army_id, out army_id2);
                                    }

                                    if (!logic.CheckIfClassExists(army_id2))
                                    {
                                        Console.WriteLine("Nem létezik a hadsereg id!");
                                        goto ArmyDoesntExist;
                                    }

                                    Console.WriteLine("Írja be a klón becenevét!");
                                    string nickname = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón tényleges nevét/gyártási kódját! (Ajánlott megadási mód: C_-5555)");
                                    string name_ = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón specialitását!");
                                    string speciality = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón születése napját! (Ajánlott megadási mód: XY BBY)");
                                    string birth_date = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón rangját!");
                                    string rank = Console.ReadLine();

                                    bool success = logic.InsertCloneData(clone_id2, class_id2, army_id2, nickname, name_, speciality, birth_date, rank);
                                    string text = success ? "Sikeres bevitel." : "Sikertelen bevitel";
                                    Console.WriteLine(text);
                                }
                                else if (dbname == "Classes")
                                {
                                    Console.WriteLine("Írd be a klassz ID-ját!");
                                    string class_id = Console.ReadLine();
                                    int class_id2;
                                    bool b2 = int.TryParse(class_id, out class_id2);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Írd be a klassz ID-ját!");
                                        class_id = Console.ReadLine();
                                        b2 = int.TryParse(class_id, out class_id2);
                                    }

                                    Console.WriteLine("Írja be a klassz nevét!");
                                    string name_ = Console.ReadLine();

                                    Console.WriteLine("Írja be a klassz fő fegycerének a nevét!");
                                    string main_weapon = Console.ReadLine();

                                    Console.WriteLine("Írja be a klassz kiegészítő felszerelését!");
                                    string utility = Console.ReadLine();

                                    Console.WriteLine("Írja be, hogy a klassz kapott-e speciális kiképzést! ('yes' vagy 'no')");
                                    string special_training = Console.ReadLine();

                                    Console.WriteLine("Írja be a klassz gyakoriságát!");
                                    string rarity = Console.ReadLine();

                                    bool success = logic.InsertClassData(class_id2, name_, main_weapon, utility, special_training, rarity);
                                    string text = success ? "Sikeres bevitel." : "Sikertelen bevitel";
                                    Console.WriteLine(text);
                                }
                                else
                                {
                                    Console.WriteLine("Hibás opció!");
                                }

                                break;
                            }

                        case 4:
                            {
                                List<string> list = logic.GetAllTableNames();
                                Console.WriteLine("Írd be a tábla nevét amiből törölni szeretnél!");
                                string dbname = Console.ReadLine();
                                while (!list.Contains(dbname))
                                {
                                    Console.WriteLine("Írd be a tábla nevét amiből törölni szeretnél!");
                                    dbname = Console.ReadLine();
                                }

                                if (dbname == "Clones")
                                {
                                    Console.WriteLine(logic.GetCloneData() + "\n");
                                }
                                else if (dbname == "Classes")
                                {
                                    Console.WriteLine(logic.GetClassData() + "\n");
                                }
                                else if (dbname == "Army")
                                {
                                    Console.WriteLine(logic.GetArmyData() + "\n");
                                }

                                Console.WriteLine("Írd be a törlendő rekord azonosítóját! (xyz)");
                                string idofval = Console.ReadLine();
                                int idofvaly;
                                bool b2 = int.TryParse(idofval, out idofvaly);
                                while (!b2)
                                {
                                    Console.WriteLine("Írd be a törlendő rekord azonosítóját! (xyz)");
                                    idofval = Console.ReadLine();
                                    b2 = int.TryParse(idofval, out idofvaly);
                                }

                                bool success;
                                if (dbname == "Clones")
                                {
                                    success = logic.DeleteCloneData(idofvaly);
                                }
                                else if (dbname == "Classes")
                                {
                                    success = logic.DeleteClassData(idofvaly);
                                }
                                else if (dbname == "Army")
                                {
                                    success = logic.DeleteArmyData(idofvaly);
                                }
                                else
                                {
                                    Console.WriteLine("Nincs ilyen opció!");
                                    return;
                                }

                                string text = success ? "Sikeres törlés." : "Sikertelen törlés";
                                Console.WriteLine(text);
                                break;
                            }

                        case 5:
                            {
                                List<string> list = logic.GetAllTableNames();
                                Console.WriteLine("Írd be a tábla nevét amiben rekordot szeretnél módosítani!");
                                string dbname = Console.ReadLine();
                                while (!list.Contains(dbname))
                                {
                                    Console.WriteLine("Írd be a tábla nevét amiben rekordot szeretnél módosítani!");
                                    dbname = Console.ReadLine();
                                }

                                if (dbname == "Clones")
                                {
                                    Console.WriteLine(logic.GetCloneData() + "\n");
                                }
                                else if (dbname == "Classes")
                                {
                                    Console.WriteLine(logic.GetClassData() + "\n");
                                }
                                else if (dbname == "Army")
                                {
                                    Console.WriteLine(logic.GetArmyData() + "\n");
                                }

                            IDDoesNotExist:
                                Console.WriteLine("Írd be a kiválasztott rekord ID-jét! (xyz)");
                                string dbid = Console.ReadLine();
                                int dbidy;
                                bool b2 = int.TryParse(dbid, out dbidy);
                                while (!b2)
                                {
                                    Console.WriteLine("Írd be a kiválasztott rekord ID-jét! (xyz)");
                                    dbid = Console.ReadLine();
                                    b2 = int.TryParse(dbid, out dbidy);
                                }

                                if (dbname == "Army")
                                {
                                    if (!logic.CheckIfArmyExists(dbidy))
                                    {
                                        Console.WriteLine("Nincs ilyen ID a " + dbname + " nevű táblában!");
                                        goto IDDoesNotExist;
                                    }

                                    Console.WriteLine(logic.GetArmyData() + "\n");
                                    Console.WriteLine("Írja be a hadsereg ID-ját! (XYZ)");

                                    string army_ID = Console.ReadLine();
                                    int army_ID2;
                                    bool b1 = int.TryParse(army_ID, out army_ID2);
                                    while (!b1)
                                    {
                                        Console.WriteLine("Írj be egy ID-t! (XYZ)");
                                        army_ID = Console.ReadLine();
                                        b1 = int.TryParse(army_ID, out army_ID2);
                                    }

                                    Console.WriteLine("Írja be a hadsereg nevét!");
                                    string name_ = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg létszámát!");
                                    string headcount = Console.ReadLine();
                                    int headcount2;
                                    bool b22 = int.TryParse(headcount, out headcount2);
                                    while (!b22)
                                    {
                                        Console.WriteLine("Írj be egy létszámot!");
                                        headcount = Console.ReadLine();
                                        b22 = int.TryParse(headcount, out headcount2);
                                    }

                                    Console.WriteLine("Írja be a hadsereg színét!");
                                    string color = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg alapításának évét! (ajánlott megadási mód: xy BBY)");
                                    string date_of_founding = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg specializációját!");
                                    string specialization = Console.ReadLine();

                                    Console.WriteLine("Írja be a hadsereg generáisának a nevét!");
                                    string name_of_general = Console.ReadLine();

                                    bool success = logic.UpdateArmyData(dbidy, army_ID2, name_, headcount2, color, date_of_founding, specialization, name_of_general);
                                    string text = success ? "Sikeres update." : "Sikertelen update";
                                    Console.WriteLine(text);
                                }

                                if (dbname == "Clones")
                                {
                                    if (!logic.CheckIfCloneExists(dbidy))
                                    {
                                        Console.WriteLine("Nincs ilyen ID a " + dbname + " nevű táblában");
                                        goto IDDoesNotExist;
                                    }

                                    Console.WriteLine("Adatbázisban lévő klónok:\n");

                                    Console.WriteLine(logic.GetCloneData() + "\n");

                                    Console.WriteLine("Írd be a klón ID-ját! (xyz)");
                                    string clone_id = Console.ReadLine();
                                    int clone_id2;
                                    bool b1 = int.TryParse(clone_id, out clone_id2);
                                    while (!b1)
                                    {
                                        Console.WriteLine("Írd be a klassz ID-ját! (xyz)");
                                        clone_id = Console.ReadLine();
                                        b1 = int.TryParse(clone_id, out clone_id2);
                                    }

                                    Console.WriteLine("Adatbázisban lévő klasszok:\n");

                                    Console.WriteLine(logic.GetClassData() + "\n");

                                    Console.WriteLine("Írja be a klón klasszának id-ját! \n");

                                    ClassDoesntExist:
                                    Console.WriteLine("Írd be a klassz ID-jét! (xyz)");
                                    string class_id = Console.ReadLine();
                                    int class_id2;
                                    bool b22 = int.TryParse(class_id, out class_id2);
                                    while (!b22)
                                    {
                                        Console.WriteLine("Írd be a klassz ID-jét! (xyz)");
                                        class_id = Console.ReadLine();
                                        b22 = int.TryParse(class_id, out class_id2);
                                    }

                                    if (!logic.CheckIfClassExists(class_id2))
                                    {
                                        Console.WriteLine("Nem létezik a klassz id!");
                                        goto ClassDoesntExist;
                                    }

                                    Console.WriteLine("Adatbázisban lévő hadseregek:\n");

                                    Console.WriteLine(logic.GetArmyData() + "\n");

                                    Console.WriteLine("Írja be a klón hadseregének id-ját! \n");

                                    ArmyDoesntExist:
                                    Console.WriteLine("Írd be a hadsereg ID-jét! (xyz)");
                                    string army_id = Console.ReadLine();
                                    int army_id2;
                                    bool b3 = int.TryParse(army_id, out army_id2);
                                    while (!b3)
                                    {
                                        Console.WriteLine("Írd be a hadsereg ID-jét! (xyz)");
                                        army_id = Console.ReadLine();
                                        b3 = int.TryParse(army_id, out army_id2);
                                    }

                                    if (!logic.CheckIfClassExists(army_id2))
                                    {
                                        Console.WriteLine("Nem létezik a hadsereg id!");
                                        goto ArmyDoesntExist;
                                    }

                                    Console.WriteLine("Írja be a klón becenevét!");
                                    string nickname = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón tényleges nevét/gyártási kódját! (Ajánlott megadási mód: C_-5555)");
                                    string name_ = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón specialitását!");
                                    string speciality = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón születése napját! (Ajánlott megadási mód: XY BBY");
                                    string birth_date = Console.ReadLine();

                                    Console.WriteLine("Írja be a klón rangját!");
                                    string rank = Console.ReadLine();

                                    bool success = logic.UpdateCloneData(dbidy, clone_id2, class_id2, army_id2, nickname, name_, speciality, birth_date, rank);
                                    string text = success ? "Sikeres bevitel." : "Sikertelen bevitel";
                                    Console.WriteLine(text);
                                }

                                if (dbname == "Classes")
                                {
                                    if (!logic.CheckIfClassExists(dbidy))
                                    {
                                        Console.WriteLine("Nincs ilyen ID a " + dbname + " nevű táblában");
                                        goto IDDoesNotExist;
                                    }

                                    Console.WriteLine("Az adatbázisban lévő klasszok: ");
                                    Console.WriteLine(logic.GetClassData() + "\n");

                                    Console.WriteLine("Írd be a klassz ID-ját! (xyz)");
                                    string class_id = Console.ReadLine();
                                    int class_id2;
                                    bool b4 = int.TryParse(class_id, out class_id2);
                                    while (!b4)
                                    {
                                        Console.WriteLine("Írd be a klassz ID-ját! (xyz)");
                                        class_id = Console.ReadLine();
                                        b4 = int.TryParse(class_id, out class_id2);
                                    }

                                    Console.WriteLine("Írja be a klassz nevét!");
                                    string name_ = Console.ReadLine();

                                    Console.WriteLine("Írja be a klassz fő fegycerének a nevét!");
                                    string main_weapon = Console.ReadLine();

                                    Console.WriteLine("Írja be a klassz kiegészítő felszerelését!");
                                    string utility = Console.ReadLine();

                                    Console.WriteLine("Írja be, hogy a klassz kapott-e speciális kiképzést! ('yes' vagy 'no')");
                                    string special_training = Console.ReadLine();

                                    Console.WriteLine("Írja be a klassz gyakoriságát!");
                                    string rarity = Console.ReadLine();

                                    bool success = logic.UpdateClassData(dbidy, class_id2, name_, main_weapon, utility, special_training, rarity);
                                    string text = success ? "Sikeres bevitel." : "Sikertelen bevitel";
                                    Console.WriteLine(text);
                                }
                                else
                                {
                                    Console.WriteLine("Nincs ilyen opció!");
                                    return;
                                }

                                break;
                            }

                        case 6:
                            {
                                Clone_Repository clone = new Clone_Repository();
                                string commanders = logic.CommandersName(clone);
                                if (commanders == null)
                                {
                                    Console.WriteLine("Nincsenek parancsnoki rangon levő klónok.");
                                }
                                else
                                {
                                    Console.WriteLine(commanders);
                                }

                                break;
                            }

                        case 7:
                            {
                                Class_Repository classes = new Class_Repository();
                                int number = logic.HowManySpecialTraining(classes);
                                Console.WriteLine(number + " klassz kapott különleges kiképzést.");

                                break;
                            }

                        case 8:
                            {
                                Army_Repository army = new Army_Repository();
                                Console.WriteLine("Az Army táblában lévő adatok:");
                                Console.WriteLine(logic.GetArmyData() + "\n");
                                decimal avg = logic.GetAverageForHeadCountOfTheArmy(army);
                                Console.WriteLine("\n\nA hadseregek látszámának átlaga: " + avg);

                                break;
                            }

                        case 9:
                            {
                                using (var client = new WebClient())
                                {
                                    XDocument.Load(V);
                                    XDocument doc = System.Xml.Linq.XDocument.Parse(V);
                                    Console.WriteLine(doc);
                                    Console.ReadLine();
                                }

                                break;
                            }

                        case 10:
                            {
                                Console.Clear();
                                Console.WriteLine("Biztosan ki akar lépni?\t(y,n)");
                                string exit = Console.ReadLine();
                                if (exit == "y")
                                {
                                    Console.Clear();
                                    Console.WriteLine("Viszlát!");
                                    this.run = false;
                                }
                                else if (exit == "n")
                                {
                                    Console.Clear();
                                    this.Help();
                                }
                                else
                                {
                                    throw new Exception();
                                }

                                break;
                            }

                        default:
                            {
                                Console.WriteLine("Hibás Opció! Listázom a lehetőségeket.");
                                this.Help();
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Hiba történt: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Displays the options of the menu.
        /// </summary>
        private void Help()
        {
            Console.Clear();
            Console.WriteLine("Válassz egy opciót (Üss be egy számot)");
            Console.WriteLine("1. Entity listázás név alapján");
            Console.WriteLine("2. Adatok listázása táblából.");
            Console.WriteLine("3. Entry hozzáadás táblához.");
            Console.WriteLine("4. Entry törlés a táblából id alapján.");
            Console.WriteLine("5. Entry update táblából, összes érték egyesével.");
            Console.WriteLine("6. Parancsnokok neveit vegye ki a klón táblából. (NON_CRUD)");
            Console.WriteLine("7. Adja meg, hogy hány darab klassz kapot különleges kiképzést. (NON_CRUD)");
            Console.WriteLine("8. Adja meg, a hadseregek létszámának átlagát. (NON_CRUD)");
            Console.WriteLine("9. Java web");
            Console.WriteLine("10. Kilépés");
        }
    }
}