﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Program starts here.
    /// </summary>
    internal class Program
    {
        private static Menu menu;

        /// <summary>
        /// This is where the program launches.
        /// </summary>
        public static void Main()
        {
            menu = new Menu();
            menu.Watcher();
        }
    }
}
