﻿namespace CloneArmy_Console_Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Cryptography.X509Certificates;
    using Newtonsoft.Json;

    public class Clone
    {
        /// <summary>
        /// Gets or sets the clone id.
        /// </summary>
        public int Clone_ID { get; set; }

        /// <summary>
        /// Gets or sets the class id.
        /// </summary>
        public int Class_ID { get; set; }

        /// <summary>
        /// Gets or sets the armmy id.
        /// </summary>
        public int Army_ID { get; set; }

        /// <summary>
        /// Gets or sets the nickname of the clone.
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// Gets or sets the name of the clone.
        /// </summary>
        public string Name_ { get; set; }

        /// <summary>
        /// Gets or sets the birth date of the clone.
        /// </summary>
        public string Birth_Date { get; set; }

        /// <summary>
        /// Gets or sets the speciality of the clone.
        /// </summary>
        public string Speciality { get; set; }

        /// <summary>
        /// Gets or sets the rank of the clone.
        /// </summary>
        public string Ranks { get; set; }

        /// <summary>
        /// Gets or sets the army of the clone.
        /// </summary>
        public virtual string ArmyName { get; set; }

        /// <summary>
        /// Gets or sets the class of the clone.
        /// </summary>
        public virtual string ClassName { get; set; }

        /// <returns></returns>
        public override string ToString()
        {
            return $"ID={this.Clone_ID}\tArmy_ID:= {this.Army_ID}\tName= {this.Name_}\tNickName= {this.NickName}\tBirthDate= {this.Birth_Date}\tSpeciality= {this.Speciality}\t" +
                $"Rank= {this.Ranks}\tArmy Name= {this.ArmyName}\tClass Name= {this.ClassName}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:49863/api/CloneApi/";
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Clone>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }

                Console.ReadLine();

                Dictionary<string, string> postData;

                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Clone.Clone_ID), "006");
                postData.Add(nameof(Clone.Class_ID), "003");
                postData.Add(nameof(Clone.Army_ID), "001");
                postData.Add(nameof(Clone.Name_), "CT-1234");
                postData.Add(nameof(Clone.NickName), "Heavy");
                postData.Add(nameof(Clone.Speciality), "close combat");
                postData.Add(nameof(Clone.Birth_Date), "32 BBY");
                postData.Add(nameof(Clone.Ranks), "Lieutenant");
                postData.Add(nameof(Clone.ArmyName), "501st Legion");
                postData.Add(nameof(Clone.ClassName), "Clone Heavy Trooper");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int cloneID = JsonConvert.DeserializeObject<List<Clone>>(json).Single(x => x.Name_ == "CT-1234").Clone_ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Clone.Clone_ID), cloneID.ToString());
                postData.Add(nameof(Clone.Class_ID), "003");
                postData.Add(nameof(Clone.Army_ID), "001");
                postData.Add(nameof(Clone.Name_), "CT-1234");
                postData.Add(nameof(Clone.NickName), "Heavy");
                postData.Add(nameof(Clone.Speciality), "close combat");
                postData.Add(nameof(Clone.Birth_Date), "32 BBY");
                postData.Add(nameof(Clone.Ranks), "Captain");
                postData.Add(nameof(Clone.ArmyName), "501st Legion");
                postData.Add(nameof(Clone.ClassName), "Clone Heavy Trooper");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + cloneID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}
