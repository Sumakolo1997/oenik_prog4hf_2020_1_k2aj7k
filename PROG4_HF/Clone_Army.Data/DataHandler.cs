﻿// <copyright file="DataHandler.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Handles the projects inner read-write method calls for the database.
    /// </summary>
    public class DataHandler : IDisposable
    {
        /// <summary>
        /// Holds the instance of the class.
        /// </summary>
        private static DataHandler instance;

        /// <summary>
        /// Holds the database model.
        /// </summary>
        private readonly CloneArmyDataEntities cloneArmyDataEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataHandler"/> class.
        /// Prevents a default instance of the <see cref="DataHandler"/> class from being created.
        /// Creates an instance for the DataHandler.
        /// </summary>
        private DataHandler()
        {
            this.cloneArmyDataEntities = new CloneArmyDataEntities();
        }

        /// <summary>
        /// Gets the instance of the DataBaseHandler.
        /// </summary>
        public static DataHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataHandler();
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets all Table's names from the database.
        /// </summary>
        public virtual List<string> TableNames
        {
            get
            {
                return this.cloneArmyDataEntities.Database.SqlQuery<string>("SELECT name FROM sys.tables ORDER BY name").ToList();
            }
        }

        /// <summary>
        /// Gets the CloneArmyDataBase instance.
        /// </summary>
        public virtual CloneArmyDataEntities CloneArmyDataBase
        {
            get { return this.cloneArmyDataEntities; }
        }

        /// <summary>
        /// Adds a new army to the database.
        /// </summary>
        /// <param name="army">Army.</param>
        public virtual void New_Army(Army army)
        {
            this.cloneArmyDataEntities.Armies.Add(army);
        }

        /// <summary>
        /// Adds a new clone to the database.
        /// </summary>
        /// <param name="clone">Clone.</param>
        public virtual void New_Clone(Clone clone)
        {
            this.cloneArmyDataEntities.Clones.Add(clone);
        }

        /// <summary>
        /// Adds a new class to the database.
        /// </summary>
        /// <param name="class">Class.</param>
        public virtual void New_Class(Class @class)
        {
            this.cloneArmyDataEntities.Classes.Add(@class);
        }

        /// <summary>
        /// Returns all current classes from the database.
        /// </summary>
        /// <returns>Class.</returns>
        public virtual List<Class> GetAllClasses()
        {
            var cquery = this.cloneArmyDataEntities.Classes.Select(x => x);
            return cquery.ToList();
        }

        /// <summary>
        /// Returns all current clones from the database.
        /// </summary>
        /// <returns>Clone.</returns>
        public virtual List<Clone> GetAllClones()
        {
            var cquery = this.cloneArmyDataEntities.Clones.Select(x => x);
            return cquery.ToList();
        }

        /// <summary>
        /// Returns all current armies from the database.
        /// </summary>
        /// <returns>Army.</returns>
        public virtual List<Army> GetAllArmies()
        {
            var cquery = this.cloneArmyDataEntities.Armies.Select(x => x);
            return cquery.ToList();
        }

        /// <summary>
        /// Deletes army from the database.
        /// </summary>
        /// <param name="army">Army.</param>
        public void Delete_Army(Army army)
        {
            if (this.cloneArmyDataEntities.Armies.Any(x => x.Army_ID == army.Army_ID))
            {
                this.cloneArmyDataEntities.Armies.Remove(army);
            }
        }

        /// <summary>
        /// Deletes clone from the database.
        /// </summary>
        /// <param name="clone">Clone.</param>
        public void Delete_Clone(Clone clone)
        {
            if (this.cloneArmyDataEntities.Clones.Any(x => x.Clone_ID == clone.Clone_ID))
            {
                this.cloneArmyDataEntities.Clones.Remove(clone);
            }
        }

        /// <summary>
        /// Deletes class from the database.
        /// </summary>
        /// <param name="class">Class.</param>
        public void Delete_Class(Class @class)
        {
            if (this.cloneArmyDataEntities.Classes.Any(x => x.Class_ID == @class.Class_ID))
            {
                this.cloneArmyDataEntities.Classes.Remove(@class);
            }
        }

        /// <summary>
        /// Saves the database.
        /// </summary>
        public virtual void SaveDatabase()
        {
            this.cloneArmyDataEntities.SaveChanges();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.cloneArmyDataEntities.Dispose();
        }
    }
}
