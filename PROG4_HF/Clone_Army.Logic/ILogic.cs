﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Clone_Army.Data;
    using Clone_Army.Repository;

    /// <summary>
    /// ILogic interface with the base methods for the logic class.
    /// /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets all table names.
        /// </summary>
        /// <returns>List of strings.</returns>
        List<string> GetAllTableNames();

        /// <summary>
        /// Inserts army data into the database.
        /// </summary>
        /// <param name="army_ID">army.</param>
        /// <param name="name_">name.</param>
        /// <param name="headCount">head.</param>
        /// <param name="color">color.</param>
        /// <param name="date_Of_Founding">date_of.</param>
        /// <param name="specialization">spec.</param>
        /// <param name="name_Of_General">name_of.</param>
        /// <returns>True/False.</returns>
        bool InsertArmyData(int army_ID, string name_, int headCount, string color, string date_Of_Founding, string specialization, string name_Of_General);

        /// <summary>
        /// Insert clone data into the database.
        /// </summary>
        /// <param name="clone_ID">clone.</param>
        /// <param name="class_ID">class.</param>
        /// <param name="army_ID">army.</param>
        /// <param name="nickname">nick.</param>
        /// <param name="name_">name.</param>
        /// <param name="speciality">spec.</param>
        /// <param name="birth_Date">birth.</param>
        /// <param name="ranks">ranks.</param>
        /// <returns>True/False.</returns>
        bool InsertCloneData(int clone_ID, int class_ID, int army_ID, string nickname, string name_, string speciality, string birth_Date, string ranks);

        /// <summary>
        /// Insert class data into the database.
        /// </summary>
        /// <param name="class_ID">class.</param>
        /// <param name="name_">name.</param>
        /// <param name="main_Weapon">main.</param>
        /// <param name="utility">uility.</param>
        /// <param name="special_training">special.</param>
        /// <param name="rarity">rarity.</param>
        /// <returns>True/False.</returns>
        bool InsertClassData(int class_ID, string name_, string main_Weapon, string utility, string special_training, string rarity);

        /// <summary>
        /// Updates the clone data.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <param name="clone_ID">clone_id.</param>
        /// <param name="class_ID">class_id</param>
        /// <param name="army_ID">army_id</param>
        /// <param name="nickname">nickname.</param>
        /// <param name="name_">name_.</param>
        /// <param name="speciality">speciality.</param>
        /// <param name="birth_Date">birth_Date.</param>
        /// <param name="ranks">ranks.</param>
        /// <returns>True/False.</returns>
        bool UpdateCloneData(int iD, int clone_ID, int class_ID, int army_ID, string nickname, string name_, string speciality, string birth_Date, string ranks);

        /// <summary>
        /// Gets the army data as a string.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetArmyData();

        /// <summary>
        /// Gets clone data as a string.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetCloneData();

        /// <summary>
        /// Gets all clone data as a list.
        /// </summary>
        /// <returns>IList<Clone>.</returns>
        IList<Clone> GetAllCloneData();

        /// <summary>
        /// Gets class data as a string-
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetClassData();

        /// <summary>
        /// Cheks if army exists.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        bool CheckIfArmyExists(int iD);

        /// <summary>
        /// Cheks if Clone exists.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        bool CheckIfCloneExists(int iD);

        /// <summary>
        /// Cheks if Class exists.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        bool CheckIfClassExists(int iD);

        /// <summary>
        /// Deletes army data.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        bool DeleteArmyData(int iD);

        /// <summary>
        /// Deletes clone data.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        bool DeleteCloneData(int iD);

        /// <summary>
        /// Deletes class data.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        bool DeleteClassData(int iD);

        /// <summary>
        /// Returns the names of the commanders.
        /// </summary>
        /// <param name="clones">clones.</param>
        /// <returns>string.</returns>
        string CommandersName(IRepository<Clone> clones);

        /// <summary>
        /// Returns the number of classes that had special training.
        /// </summary>
        /// <param name="classes">classes.</param>
        /// <returns>integer.</returns>
        int HowManySpecialTraining(IRepository<Class> classes);

        /// <summary>
        /// Returns the average fro headcount of the armies.
        /// </summary>
        /// <param name="armies">armies.</param>
        /// <returns>decimal.</returns>
        decimal GetAverageForHeadCountOfTheArmy(IRepository<Army> armies);

        /// <summary>
        /// Returns one clone from the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>Clone.</returns>
        Clone GetClone(int id);
    }
}
