﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Clone_Army.Data;
    using Clone_Army.Repository;

    /// <summary>
    /// Constains the logic database methods.
    /// </summary>
    public class Logic : ILogic
    {
        private readonly RepositoryHelper helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Creates the logic, and initializes it.
        /// </summary>
        public Logic()
        {
            this.helper = new RepositoryHelper();
        }

        /// <summary>
        /// Gets the RepositoryHelper instance.
        /// </summary>
        public virtual RepositoryHelper Helper
        {
            get
            {
                return this.helper;
            }
        }

        /// <summary>
        /// Update's army data to the specified parameters.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <param name="army_ID">army.</param>
        /// <param name="name_">name.</param>
        /// <param name="headCount">head.</param>
        /// <param name="color">color.</param>
        /// <param name="date_Of_Founding">date.</param>
        /// <param name="specialization">spec.</param>
        /// <param name="name_Of_General">name_of.</param>
        /// <returns>True/False.</returns>
        public bool UpdateArmyData(int iD, int army_ID, string name_, int headCount, string color, string date_Of_Founding, string specialization, string name_Of_General)
        {
            return this.Helper.Army_Repository.Update(iD, army_ID, name_, headCount, color, date_Of_Founding, specialization, name_Of_General);
        }

        /// <summary>
        /// Update's clone data to the specified parameters.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <param name="clone_ID">clone.</param>
        /// <param name="class_ID">class.</param>
        /// <param name="army_ID">army.</param>
        /// <param name="nickname">nickname.</param>
        /// <param name="name_">name.</param>
        /// <param name="speciality">spec.</param>
        /// <param name="birth_Date">birth.</param>
        /// <param name="ranks">ranks.</param>
        /// <returns>True/False.</returns>
        public bool UpdateCloneData(int iD, int clone_ID, int class_ID, int army_ID, string nickname, string name_, string speciality, string birth_Date, string ranks)
        {
            return this.Helper.Clone_Repository.Update(iD, clone_ID, class_ID, army_ID, nickname, name_, speciality, birth_Date, ranks);
        }

        /// <summary>
        /// Update's class data to the specified parameters.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <param name="class_ID">class.</param>
        /// <param name="name_">name.</param>
        /// <param name="main_Weapon">main.</param>
        /// <param name="utility">utility.</param>
        /// <param name="special_training">special.</param>
        /// <param name="rarity">rarity.</param>
        /// <returns>True/False.</returns>
        public bool UpdateClassData(int iD, int class_ID, string name_, string main_Weapon, string utility, string special_training, string rarity)
        {
            return this.Helper.Class_Repository.Update(iD, class_ID, name_, main_Weapon, utility, special_training, rarity);
        }

        /// <summary>
        /// Checks if the army exists.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        public bool CheckIfArmyExists(int iD)
        {
            return this.Helper.Army_Repository.CheckIfArmyExists(iD);
        }

        /// <summary>
        /// Checks if the class exists.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        public bool CheckIfClassExists(int iD)
        {
            return this.Helper.Class_Repository.CheckIfCLassExists(iD);
        }

        /// <summary>
        /// Checks if the clone exists.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        public bool CheckIfCloneExists(int iD)
        {
            return this.Helper.Clone_Repository.CheckIfCLoneExists(iD);
        }

        /// <summary>
        /// Deletes an army data from the database.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        public bool DeleteArmyData(int iD)
        {
            return this.Helper.Army_Repository.Delete(iD);
        }

        /// <summary>
        /// Deletes a class data from the database.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        public bool DeleteClassData(int iD)
        {
            return this.Helper.Class_Repository.Delete(iD);
        }

        /// <summary>
        /// Deletes a clone data from the database.
        /// </summary>
        /// <param name="iD">id.</param>
        /// <returns>True/False.</returns>
        public bool DeleteCloneData(int iD)
        {
            return this.Helper.Clone_Repository.Delete(iD);
        }

        /// <summary>
        /// Returns all table names from the database.
        /// </summary>
        /// <returns>True/False.</returns>
        public List<string> GetAllTableNames()
        {
            return this.Helper.GetAllableNames();
        }

        /// <summary>
        /// Returns the army data as a string.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetArmyData()
        {
            return this.Helper.Army_Repository.GetTableContents();
        }

        /// <summary>
        /// Returns the class data as a string.
        /// </summary>
        /// <returns>True/False.</returns>
        public StringBuilder GetClassData()
        {
            return this.Helper.Class_Repository.GetTableContents();
        }

        /// <summary>
        /// Returns the clone data as a string.
        /// </summary>
        /// <returns>True/False.</returns>
        public StringBuilder GetCloneData()
        {
            return this.Helper.Clone_Repository.GetTableContents();
        }

        /// <summary>
        /// Returns all clone data as a list.
        /// </summary>
        /// <returns>IList<Clone>.</Clone></returns>
        public IList<Clone> GetAllCloneData()
        {
            return this.Helper.Clone_Repository.GetAllClones();
        }

        /// <summary>
        /// Inserts army data to the database.
        /// </summary>
        /// <param name="army_ID">army.</param>
        /// <param name="name_">name.</param>
        /// <param name="headCount">head.</param>
        /// <param name="color">color.</param>
        /// <param name="date_Of_Founding">date.</param>
        /// <param name="specialization">spec.</param>
        /// <param name="name_Of_General">name_of.</param>
        /// <returns>True/False.</returns>
        public bool InsertArmyData(int army_ID, string name_, int headCount, string color, string date_Of_Founding, string specialization, string name_Of_General)
        {
            return this.Helper.Army_Repository.Insert(army_ID, name_, headCount, color, date_Of_Founding, specialization, name_Of_General);
        }

        /// <summary>
        /// Inserts class data to the database.
        /// </summary>
        /// <param name="class_ID">id-</param>
        /// <param name="name_">name.</param>
        /// <param name="main_Weapon">main.</param>
        /// <param name="utility">utility.</param>
        /// <param name="special_training">special.</param>
        /// <param name="rarity">rarity.</param>
        /// <returns>True/False.</returns>
        public bool InsertClassData(int class_ID, string name_, string main_Weapon, string utility, string special_training, string rarity)
        {
            return this.Helper.Class_Repository.Insert(class_ID, name_, main_Weapon, utility, special_training, rarity);
        }

        /// <summary>
        /// Inserts clone data to the database.
        /// </summary>
        /// <param name="clone_ID">id.</param>
        /// <param name="class_ID">class.</param>
        /// <param name="army_ID">army.</param>
        /// <param name="nickname">nickname.</param>
        /// <param name="name_">name.</param>
        /// <param name="speciality">spec.</param>
        /// <param name="birth_Date">birth.</param>
        /// <param name="ranks">ranks.</param>
        /// <returns>True/False.</returns>
        public bool InsertCloneData(int clone_ID, int class_ID, int army_ID, string nickname, string name_, string speciality, string birth_Date, string ranks)
        {
            return this.Helper.Clone_Repository.Insert(clone_ID, class_ID, army_ID, nickname, name_, speciality, birth_Date, ranks);
        }

        /// <summary>
        /// Returns the names of the clones whose rank is commander.
        /// </summary>
        /// <param name="clones">clones.</param>
        /// <returns>string.</returns>
        public string CommandersName(IRepository<Clone> clones)
        {
            var parancsnokok = from c in clones.List_Table()
                               where c.Ranks == "Commander"
                               select c.NickName;

            return string.Join("\n", parancsnokok);
        }

        /// <summary>
        /// Returns the number of specially trained classes.
        /// </summary>
        /// <param name="classes">classes.</param>
        /// <returns>integer.</returns>
        public int HowManySpecialTraining(IRepository<Class> classes)
        {
            int speciallyTrained = (from c in classes.List_Table()
                                    where c.Special_Training == "yes"
                                    select c).Count();
            return speciallyTrained;
        }

        /// <summary>
        /// Returns the average for headcount of the armies.
        /// </summary>
        /// <param name="armies">armies.</param>
        /// <returns>decimal.</returns>
        public decimal GetAverageForHeadCountOfTheArmy(IRepository<Army> armies)
        {
            decimal sum = (from a in armies.List_Table() select (int)a.HeadCount).Sum();
            decimal armiesNumber = armies.List_Table().Count();
            return sum / armiesNumber;
        }

        /// <summary>
        /// Returns one clone from the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>Clone.</returns>
        public Clone GetClone(int id)
        {
            return this.Helper.Clone_Repository.GetOneClone(id);
        }
    }
}
