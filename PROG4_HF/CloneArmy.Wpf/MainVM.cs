﻿namespace CloneArmy.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Remoting.Proxies;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;

    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private CloneVM selectedClone;
        private ObservableCollection<CloneVM> allClones;

        public ObservableCollection<CloneVM> AllClones
        {
            get { return this.allClones; }
            set { this.Set(ref this.allClones, value); }
        }


        public CloneVM SelectedClone
        {
            get { return this.selectedClone; }
            set { this.Set(ref this.selectedClone, value); }
        }

        public ICommand AddCmd { get; private set; }

        public ICommand DelCmd { get; private set; }

        public ICommand ModCmd { get; private set; }

        public ICommand LoadCmd { get; private set; }

        public Func<CloneVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            this.logic = new MainLogic();

            this.DelCmd = new RelayCommand(() => this.logic.ApiDelClone(this.selectedClone));
            this.AddCmd = new RelayCommand(() => this.logic.EditClone(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditClone(this.selectedClone, this.EditorFunc));
            this.LoadCmd = new RelayCommand(() => this.AllClones = new ObservableCollection<CloneVM>(this.logic.ApiGetClones()));


        }

    }
}
