﻿namespace CloneArmy.Wpf
{
    using System;
    using System.CodeDom;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    class CloneVM : ObservableObject
    {
        private int clone_ID;
        private int class_ID;
        private int army_ID;
        private string name_;
        private string nickName;
        private string birth_Date;
        private string speciality;
        private string ranks;
        private string armyName;
        private string className;

        public void CopyFrom(CloneVM other)
        {
            if (other == null) return;
            this.Clone_ID = other.clone_ID;
            this.Army_ID = other.army_ID;
            this.Class_ID = other.class_ID;
            this.Name_ = other.name_;
            this.NickName = other.nickName;
            this.Birth_Date = other.birth_Date;
            this.Speciality = other.speciality;
            this.Ranks = other.ranks;
            this.ArmyName = other.armyName;
            this.ClassName = other.className;
        }

        public string ClassName
        {
            get { return className; }
            set { this.Set(ref this.className, value); }
        }

        public string ArmyName
        {
            get { return armyName; }
            set { this.Set(ref this.armyName, value); }
        }

        public string Ranks
        {
            get { return ranks; }
            set { this.Set(ref this.ranks, value); }
        }

        public string Speciality
        {
            get { return speciality; }
            set { this.Set(ref this.speciality, value); }
        }

        public string Birth_Date
        {
            get { return birth_Date; }
            set { this.Set(ref this.birth_Date, value); }
        }

        public string NickName
        {
            get { return nickName; }
            set { this.Set(ref this.nickName, value); }
        }

        public string Name_
        {
            get { return name_; }
            set { this.Set(ref this.name_, value); }
        }

        public int Army_ID
        {
            get { return army_ID; }
            set { this.Set(ref this.army_ID, value); }
        }

        public int Class_ID
        {
            get { return class_ID; }
            set { this.Set(ref this.class_ID, value); }
        }

        public int Clone_ID
        {
            get { return clone_ID; }
            set { this.Set(ref this.clone_ID, value); }
        }

    }
}
