﻿namespace CloneArmy.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    class MainLogic
    {
        string url = "http://localhost:49863/api/CloneApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "CloneResult");

        }

        public List<CloneVM> ApiGetClones()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<CloneVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelClone(CloneVM clone)
        {
            bool success = false;
            if (clone != null)
            {
                string json = client.GetStringAsync(url + "del/" + clone.Clone_ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }

            SendMessage(success);
        }

        bool ApiEditClone(CloneVM clone, bool isEditing)
        {
            if (clone == null)
            {
                return false;
            }

            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(CloneVM.Clone_ID), clone.Clone_ID.ToString());
            }

            postData.Add(nameof(CloneVM.Class_ID), clone.Class_ID.ToString());
            postData.Add(nameof(CloneVM.Army_ID), clone.Army_ID.ToString());
            postData.Add(nameof(CloneVM.Name_), clone.Name_.ToString());
            postData.Add(nameof(CloneVM.NickName), clone.NickName.ToString());
            postData.Add(nameof(CloneVM.Birth_Date), clone.Birth_Date.ToString());
            postData.Add(nameof(CloneVM.Speciality), clone.Speciality.ToString());
            postData.Add(nameof(CloneVM.Ranks), clone.Ranks.ToString());
            postData.Add(nameof(CloneVM.ArmyName), clone.ArmyName.ToString());
            postData.Add(nameof(CloneVM.ClassName), clone.ClassName.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);

            return (bool)obj["OperationResult"];
        }

        public void EditClone(CloneVM clones, Func<CloneVM, bool> editor)
        {
            CloneVM clone = new CloneVM();
            if (clones != null)
            {
                clone.CopyFrom(clones);
            }

            bool? success = editor?.Invoke(clone);

            if (success == true)
            {
                if (clones != null)
                {
                    success = ApiEditClone(clone, true);
                }
                else
                {
                    success = ApiEditClone(clone, false);
                }
            }

            SendMessage(success == true);
        }
    }
}
