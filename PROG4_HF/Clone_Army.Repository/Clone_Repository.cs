﻿// <copyright file="Clone_Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Clone_Army.Data;

    /// <summary>
    /// Class Repository handler of the Clone class.
    /// </summary>
    public class Clone_Repository : IRepository<Clone>
    {
        /// <summary>
        /// Returns all clones as queryable.
        /// </summary>
        /// <returns>Clone.</returns>
        public virtual IQueryable<Clone> List_Table()
        {
            return DataHandler.Instance.GetAllClones().AsQueryable<Clone>();
        }

        /// <summary>
        /// Inserts Clone into the database.
        /// </summary>
        /// <param name="clone_ID">clone.</param>
        /// <param name="class_ID">class.</param>
        /// <param name="army_ID">army.</param>
        /// <param name="nickname">nick.</param>
        /// <param name="name_">name.</param>
        /// <param name="speciality">speciality.</param>
        /// <param name="birth_Date">birth.</param>
        /// <param name="ranks">ranks.</param>
        /// <returns>True/False.</returns>
        public bool Insert(int clone_ID, int class_ID, int army_ID, string nickname, string name_, string speciality, string birth_Date, string ranks)
        {
            Clone clone = new Clone
            {
                Clone_ID = clone_ID,
                Class_ID = class_ID,
                Army_ID = army_ID,
                NickName = nickname,
                Name_ = name_,
                Speciality = speciality,
                Birth_Date = birth_Date,
                Ranks = ranks
            };
            DataHandler.Instance.New_Clone(clone);
            DataHandler.Instance.SaveDatabase();
            return true;
        }

        /// <summary>
        /// Updates the Clone's repository with the given parameters.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="clone_ID">clone.</param>
        /// <param name="class_ID">class.</param>
        /// <param name="army_ID">army.</param>
        /// <param name="nickname">nick.</param>
        /// <param name="name_">name.</param>
        /// <param name="speciality">speciality.</param>
        /// <param name="birth_Date">birth.</param>
        /// <param name="ranks">ranks.</param>
        /// <returns>Returns False/True.</returns>
        public bool Update(int id, int clone_ID, int class_ID, int army_ID, string nickname, string name_, string speciality, string birth_Date, string ranks)
        {
            List<Clone> query = this.List_Table().Where(x => x.Clone_ID == id).Select(y => y).ToList();
            if (query.Count > 0)
            {
                Clone clone = query[0];
                clone.Clone_ID = clone_ID;
                clone.Class_ID = class_ID;
                clone.Army_ID = army_ID;
                clone.NickName = nickname;
                clone.Name_ = name_;
                clone.Speciality = speciality;
                clone.Birth_Date = birth_Date;
                clone.Ranks = ranks;

                DataHandler.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Deletes clone from the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool Delete(int id)
        {
            List<Clone> query = this.List_Table().Where(x => x.Clone_ID == id).Select(y => y).ToList();
            if (query.Count > 0)
            {
                DataHandler.Instance.Delete_Clone(query[0]);
                DataHandler.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns one clone according to it's ID.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>Clone.</returns>
        public Clone GetOneClone(int id)
        {
            List<Clone> query = this.List_Table().Where(x => x.Clone_ID == id).Select(y => y).ToList();
            if (query.Count > 0)
            {
                return query.First();
            }

            return null;
        }

        /// <summary>
        /// Returns the repository as a string.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetTableContents()
        {
            List<Clone> temp = this.List_Table().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var x in temp)
            {
                builder.AppendLine("-* Clone_ID: " + x.Clone_ID + " | Class_ID: " + x.Class_ID + " | Army_ID: " + x.Army_ID + " | Name: " + x.Name_ + " | NickName: "
                    + x.NickName + " | Speciality: " + x.Speciality + " | Birth_Date: " + x.Birth_Date + " | Ranks: " + x.Ranks);
            }

            return builder;
        }

        /// <summary>
        /// Returns all clones as a list.
        /// </summary>
        /// <returns>IList<Clone>.</Clone></returns>
        public IList<Clone> GetAllClones()
        {
            return DataHandler.Instance.GetAllClones();
        }

        /// <summary>
        /// Checks if the clone exists in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool CheckIfCLoneExists(int id)
        {
            return this.List_Table().Any(x => x.Clone_ID == id);
        }
    }
}
