﻿// <copyright file="RepositoryHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Clone_Army.Data;

    /// <summary>
    /// This helper support getting the specific repositories.
    /// </summary>
    public class RepositoryHelper
    {
        private readonly Army_Repository armyrepo;
        private readonly Clone_Repository clonerepo;
        private readonly Class_Repository classrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryHelper"/> class.
        /// This constructor creates each instance for the repositories.
        /// </summary>
        public RepositoryHelper()
        {
            this.armyrepo = new Army_Repository();
            this.clonerepo = new Clone_Repository();
            this.classrepo = new Class_Repository();
        }

        /// <summary>
        /// Gets the army repository.
        /// </summary>
        public virtual Army_Repository Army_Repository
        {
            get { return this.armyrepo; }
        }

        /// <summary>
        /// Gets the clone repository.
        /// </summary>
        public virtual Clone_Repository Clone_Repository
        {
            get { return this.clonerepo; }
        }

        /// <summary>
        /// Gets the class repository.
        /// </summary>
        public virtual Class_Repository Class_Repository
        {
            get { return this.classrepo; }
        }

        /// <summary>
        /// Returns all the table names.
        /// </summary>
        /// <returns>List of strings.</returns>
        public List<string> GetAllableNames()
        {
            return DataHandler.Instance.TableNames;
        }
    }
}
