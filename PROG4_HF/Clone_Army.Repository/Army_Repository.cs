﻿// <copyright file="Army_Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Clone_Army.Data;

    /// <summary>
    /// Class Repository handler of the Army class.
    /// </summary>
    public class Army_Repository : IRepository<Army>
    {
        /// <summary>
        /// Returns all armies as queryable.
        /// </summary>
        /// <returns>Army.</returns>
        public virtual IQueryable<Army> List_Table()
        {
            return DataHandler.Instance.GetAllArmies().AsQueryable<Army>();
        }

        /// <summary>
        /// Inserts Class into the database.
        /// </summary>
        /// <param name="army_iD">army.</param>
        /// <param name="name_">name_.</param>
        /// <param name="headCount">head.</param>
        /// <param name="color">color.</param>
        /// <param name="date_Of_Founding">date.</param>
        /// <param name="specialization">spec.</param>
        /// <param name="name_Of_General">name_of.</param>
        /// <returns>True/False.</returns>
        public bool Insert(int army_iD, string name_, int headCount, string color, string date_Of_Founding, string specialization, string name_Of_General)
        {
            Army army = new Army
            {
                Army_ID = army_iD,
                Name_ = name_,
                HeadCount = headCount,
                Color = color,
                Date_Of_Founding = date_Of_Founding,
                Specialization = specialization,
                Name_Of_General = name_Of_General,
            };
            DataHandler.Instance.New_Army(army);
            DataHandler.Instance.SaveDatabase();
            return true;
        }

        /// <summary>
        /// Updates the Army's repository with the given parameters.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="army_iD">class.</param>
        /// <param name="name_">name_.</param>
        /// <param name="headCount">head.</param>
        /// <param name="color">color.</param>
        /// <param name="date_Of_Founding">date.</param>
        /// <param name="specialization">spec.</param>
        /// <param name="name_Of_General">name_of.</param>
        /// <returns>Returns False/True.</returns>
        public bool Update(int id, int army_iD, string name_, int headCount, string color, string date_Of_Founding, string specialization, string name_Of_General)
        {
            List<Army> query = this.List_Table().Where(x => x.Army_ID == id).Select(y => y).ToList();
            if (query.Count > 0)
            {
                Army army = query[0];
                army.Army_ID = army_iD;
                army.Name_ = name_;
                army.HeadCount = headCount;
                army.Color = color;
                army.Date_Of_Founding = date_Of_Founding;
                army.Specialization = specialization;
                army.Name_Of_General = name_Of_General;

                DataHandler.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Deletes army from the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool Delete(int id)
        {
            List<Army> query = this.List_Table().Where(x => x.Army_ID == id).Select(y => y).ToList();
            if (query.Count > 0)
            {
                DataHandler.Instance.Delete_Army(query[0]);
                DataHandler.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the repository as a string.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetTableContents()
        {
            List<Army> temp = this.List_Table().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var x in temp)
            {
                builder.AppendLine("-* Army_ID: " + x.Army_ID + " | Name: " + x.Name_ + " | HeadCount: " + x.HeadCount + " | Color: "
                    + x.Color + " | Date_Of_Founding: " + x.Date_Of_Founding + " | Specialization: "
                    + x.Specialization + " | Name_Of_General: " + x.Name_Of_General);
            }

            return builder;
        }

        /// <summary>
        /// Checks if the army exists in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool CheckIfArmyExists(int id)
        {
            return this.List_Table().Any(x => x.Army_ID == id);
        }
    }
}
