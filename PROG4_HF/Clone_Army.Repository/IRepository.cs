﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Initialization interface for the Repositories.
    /// </summary>
    /// <typeparam name="T">Any type of repository.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Returns all the elements of the repository.
        /// </summary>
        /// <returns>T type query.</returns>
        IQueryable<T> List_Table();

        /// <summary>
        /// Deletes the specific element.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        bool Delete(int id);

        /// <summary>
        /// Returns the repository contents as a string.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetTableContents();
    }
}
