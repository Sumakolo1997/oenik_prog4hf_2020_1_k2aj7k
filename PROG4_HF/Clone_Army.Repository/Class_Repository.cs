﻿// <copyright file="Class_Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Clone_Army.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Clone_Army.Data;

    /// <summary>
    /// Class Repository handler of the Class class.
    /// </summary>
    public class Class_Repository : IRepository<Class>
    {
        /// <summary>
        /// Returns all classes as queryable.
        /// </summary>
        /// <returns>Class.</returns>
        public virtual IQueryable<Class> List_Table()
        {
            return DataHandler.Instance.GetAllClasses().AsQueryable<Class>();
        }

        /// <summary>
        /// Inserts Class into the database.
        /// </summary>
        /// <param name="class_ID">class.</param>
        /// <param name="name_">name.</param>
        /// <param name="main_Weapon">main_w.</param>
        /// <param name="utility">utility.</param>
        /// <param name="special_training">special.</param>
        /// <param name="rarity">rarity.</param>
        /// <returns>True/False.</returns>
        public bool Insert(int class_ID, string name_, string main_Weapon, string utility, string special_training, string rarity)
        {
            Class @class = new Class
            {
                Class_ID = class_ID,
                Name_ = name_,
                Main_Weapon = main_Weapon,
                Utility = utility,
                Special_Training = special_training,
                Rarity = rarity
            };
            DataHandler.Instance.New_Class(@class);
            DataHandler.Instance.SaveDatabase();
            return true;
        }

        /// <summary>
        /// Updates the Class's repository with the given parameters.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="class_ID">class.</param>
        /// <param name="name_">name.</param>
        /// <param name="main_Weapon">main_w.</param>
        /// <param name="utility">utility.</param>
        /// <param name="special_training">special.</param>
        /// <param name="rarity">rarity.</param>
        /// <returns>Returns False/True.</returns>
        public bool Update(int id, int class_ID, string name_, string main_Weapon, string utility, string special_training, string rarity)
        {
            List<Class> query = this.List_Table().Where(x => x.Class_ID == id).Select(y => y).ToList();
            if (query.Count > 0)
            {
                Class @class = query[0];
                @class.Class_ID = class_ID;
                @class.Name_ = name_;
                @class.Main_Weapon = main_Weapon;
                @class.Utility = utility;
                @class.Special_Training = special_training;
                @class.Rarity = rarity;

                DataHandler.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Deletes class from the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool Delete(int id)
        {
            List<Class> query = this.List_Table().Where(x => x.Class_ID == id).Select(y => y).ToList();
            if (query.Count > 0)
            {
                DataHandler.Instance.Delete_Class(query[0]);
                DataHandler.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the repository as a string.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetTableContents()
        {
            List<Class> temp = this.List_Table().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var x in temp)
            {
                builder.AppendLine("-* Class_ID: " + x.Class_ID + " | Name: " + x.Name_ + " | Main_Weapon: " + x.Main_Weapon +
                    " | Utility: " + x.Utility + " | Special_Training: " + x.Special_Training + " | Rarity: " + x.Rarity);
            }

            return builder;
        }

        /// <summary>
        /// Checks if the class exists in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool CheckIfCLassExists(int id)
        {
            return this.List_Table().Any(x => x.Class_ID == id);
        }
    }
}
