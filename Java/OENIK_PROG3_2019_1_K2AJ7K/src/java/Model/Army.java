package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Army")
@XmlAccessorType(XmlAccessType.FIELD)
public class Army implements Serializable{

    
    private int next_ID;
    @XmlElement
    private int army_ID;

    public void setArmy_ID(int army_ID) {
        this.army_ID = army_ID;
    }

    public int getArmy_ID() {
        return army_ID;
    }
    @XmlElement
    private int headCount;
    @XmlElement
    private String Name_;
    @XmlElement
    private String Color;
    @XmlElement
    private String Date_OF_Founding;
    @XmlElement
    private String Specialization;
    @XmlElement
    private String Name_Of_General;
    @XmlElement
    private List<Clone> clones = new ArrayList<>();
    
    public List<Clone> getClones() {
        return clones;
    }
    
    
    public int getHeadCount() {
        return headCount;
    }

    public void setHeadCount(int headCount) {
        this.headCount = headCount;
    }

    public String getName_() {
        return Name_;
    }

    public void setName_(String Name_) {
        this.Name_ = Name_;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getDate_OF_Founding() {
        return Date_OF_Founding;
    }

    public void setDate_OF_Founding(String Date_OF_Founding) {
        this.Date_OF_Founding = Date_OF_Founding;
    }

    public String getSpecialization() {
        return Specialization;
    }

    public void setSpecialization(String Specialization) {
        this.Specialization = Specialization;
    }

    public String getName_Of_General() {
        return Name_Of_General;
    }

    public void setName_Of_General(String Name_Of_General) {
        this.Name_Of_General = Name_Of_General;
    }

    public Army() {
        this.army_ID = next_ID++;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.army_ID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Army other = (Army) obj;
        if (this.army_ID != other.army_ID) {
            return false;
        }
        return true;
    }

   
}
