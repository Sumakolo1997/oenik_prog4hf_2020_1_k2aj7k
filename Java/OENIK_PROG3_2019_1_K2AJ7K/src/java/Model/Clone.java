package Model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "clone")
@XmlAccessorType(XmlAccessType.FIELD)
public class Clone implements Serializable{
    private static int next_ID;
    @XmlElement 
    private int Clone_ID;
    @XmlElement 
    private int Class_ID;
    @XmlElement 
    private int Army_ID;
    @XmlElement 
    private String NickName;
    @XmlElement 
    private String Name_;
    @XmlElement 
    private String Speciality;
    @XmlElement 
    private String Birth_Date;
    @XmlElement 
    private String Ranks;

    public Clone() {
        this.Clone_ID = next_ID++;
    }

    public int getClone_ID() {
        return Clone_ID;
    }

    public void setClone_ID(int Clone_ID) {
        this.Clone_ID = Clone_ID;
    }

    public int getClass_ID() {
        return Class_ID;
    }

    public void setClass_ID(int Class_ID) {
        this.Class_ID = Class_ID;
    }

    public int getArmy_ID() {
        return Army_ID;
    }

    public void setArmy_ID(int Army_ID) {
        this.Army_ID = Army_ID;
    }

    public String getNickName() {
        return NickName;
    }

    public void setNickName(String NickName) {
        this.NickName = NickName;
    }

    public String getName_() {
        return Name_;
    }

    public void setName_(String Name_) {
        this.Name_ = Name_;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public void setSpeciality(String Speciality) {
        this.Speciality = Speciality;
    }

    public String getBirth_Date() {
        return Birth_Date;
    }

    public void setBirth_Date(String Birth_Date) {
        this.Birth_Date = Birth_Date;
    }

    public String getRanks() {
        return Ranks;
    }

    public void setRanks(String Ranks) {
        this.Ranks = Ranks;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.Clone_ID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Clone other = (Clone) obj;
        if (this.Clone_ID != other.Clone_ID) {
            return false;
        }
        return true;
    }

    
}
