package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "queriedArmies")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueriedArmies implements Serializable{
    
    @XmlElement(name = "army")
    private Army army;
  
    @XmlElement(name = "clones")
    private List<Clone> clones;
    
     public Army getArmy() {
        return army;
    }

    public List<Clone> getClones() {
        return clones;
    }
    
    public void setArmy(Army army){
        this.army = army;
    }

    
    public void setQueriedList(ArrayList<Clone> list){
        this.clones = list;
    }
}
