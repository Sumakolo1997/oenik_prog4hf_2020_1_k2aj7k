package Controller;

import Model.Army;
import Model.Clone;
import Model.QueriedArmies;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Clone_Army_Service implements Serializable{

    private static Clone_Army_Service instance;

    public Clone_Army_Service() {
        Army army = new Army();
        army.setArmy_ID(004);
        army.setName_("NightWalkers");
        army.setHeadCount(420);
        army.setColor("green");
        army.setDate_OF_Founding("23 BBY");
        army.setSpecialization("grinding");
        army.setName_Of_General("Snoop");
        
        
        Clone clone = new Clone();
        clone.setName_("CT-4200");
        clone.setClone_ID(007);
        clone.setArmy_ID(003);
        clone.setClass_ID(001);
        clone.setNickName("Smoker");
        clone.setBirth_Date("21 BBY");
        clone.setSpeciality("living");
        clone.setRanks("Commander");
        army.getClones().add(clone);
        armies.add(army);
        
    }
    
    public static Clone_Army_Service getInstance(){
        if (instance == null) {
            instance = new Clone_Army_Service();
        }
        return instance;
    }

    
    @XmlElement
    private List<Army> armies = new ArrayList<>();

    public List<Army> getArmies() {
        return armies;
    }
    
    public QueriedArmies getQueriedArmy(String army){
        QueriedArmies local = new QueriedArmies();
        for (Army b : armies) {
            if(b.getName_().toLowerCase().trim().equals(army.toLowerCase().trim())){
            local.setArmy(b);
            }
        }
        
        return local;
    }
    
    
}
